class RouteList {
  static const String notesList = '/';
  static const String noteDetail = '/note-detail';
  static const String noteDetailRubber = '/note-detail-rubber';
}
