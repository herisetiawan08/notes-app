import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_event.dart';
import 'package:notes/common/bloc/notes_bloc/notes_state.dart';
import 'package:notes/domain/entities/note_item_entity.dart';

class NotesBloc extends Bloc<NotesEvent, NotesState> {
  NotesBloc() : super(NotesInitState());

  @override
  Stream<NotesState> mapEventToState(NotesEvent event) async* {
    switch (event.runtimeType) {
      case GetNotesEvent:
        yield* _mapGetToState(event);
        break;
      case UpdateNoteEvent:
        yield* _mapUpdateToState(event);
        break;
      default:
    }
  }

  Stream<NotesState> _mapGetToState(GetNotesEvent event) async* {
    yield NotesLoadingState(state.notes);
    final notes = <NoteItemEntity>[];
    yield NotesLoadedState(notes);
  }

  Stream<NotesState> _mapUpdateToState(UpdateNoteEvent event) async* {
    final notes = state.notes;
    print('isValid : ${event.entity.isValid}');
    if (event.entity.isValid) {
      notes
        ..removeWhere((note) => note.key == event.entity.key)
        ..add(event.entity);
    }
    yield NotesLoadedState(notes);
  }
}
