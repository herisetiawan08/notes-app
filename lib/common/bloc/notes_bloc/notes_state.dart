import 'package:notes/domain/entities/note_item_entity.dart';

abstract class NotesState {
  final List<NoteItemEntity> notes;
  NotesState(this.notes);
}

class NotesInitState extends NotesState {
  NotesInitState() : super([]);
}

class NotesLoadingState extends NotesState {
  NotesLoadingState(List<NoteItemEntity> notes) : super(notes);
}

class NotesLoadedState extends NotesState {
  NotesLoadedState(List<NoteItemEntity> notes) : super(notes);
}
