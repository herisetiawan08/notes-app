import 'package:flutter/material.dart';

class NoteOptionEntity {
  final String text;
  final NoteOptionEnum type;
  final Color color;

  NoteOptionEntity({
    this.text,
    this.type,
    this.color,
  });
}

enum NoteOptionEnum {
  editTitle,
  addWidget,
  noteStyle,
  deleteNote,
}
