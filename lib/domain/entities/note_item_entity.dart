import 'package:flutter/material.dart';
import 'package:notes/domain/entities/note_content_entity.dart';

class NoteItemEntity {
  String key;
  String createdAt;
  String title;
  Color titleColor;
  Color bgColor;
  String category;
  List<NoteContentEntity> contents;

  NoteItemEntity({
    this.key,
    this.createdAt,
    this.title,
    this.titleColor,
    this.bgColor,
    this.category,
    this.contents,
  }) {
    key ??= createdAt;
  }

  bool get isValid =>
      key != null &&
      createdAt != null &&
      title != null &&
      titleColor != null &&
      bgColor != null &&
      category != null
      // &&
      // contents != null &&
      // contents.isNotEmpty  
      ;
}
