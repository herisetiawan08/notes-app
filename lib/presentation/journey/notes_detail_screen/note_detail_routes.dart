import 'package:flutter/material.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/common/navigation/custom_routes.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_screen.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_detail_rubber_widget.dart';

class NoteDetailRoutes {
  static Map<String, WidgetBuilder> getAll() {
    addRubber();
    return {
      RouteList.noteDetail: (context) => NoteDetailScreen(),
    };
  }

  static void addRubber() => CustomRoute.getPrefix(CustomRoutes.rubber).addAll({
        RouteList.noteDetailRubber: (settings) =>
            NoteDetailRubberWidget(settings),
      });
}
