import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_event.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_state.dart';

class NoteDetailBloc extends Bloc<NoteDetailEvent, NoteDetailState> {
  NoteDetailBloc() : super(NoteDetailInitState());

  @override
  Stream<NoteDetailState> mapEventToState(NoteDetailEvent event) async* {
    switch (event.runtimeType) {
      case NoteDetailSetEntityEvent:
        yield* mapSetEventToState(event);
        break;
      case NoteDetailUpdateTitleEvent:
        yield* mapUpdateTitleEventToState(event);
        break;
      case NoteDetailUpdateContentsEvent:
        yield* mapUpdateContentsEventToState(event);
        break;
      default:
    }
  }

  Stream<NoteDetailState> mapSetEventToState(
      NoteDetailSetEntityEvent event) async* {
    yield NoteDetailEditedState(
      entity: event.entity,
    );
  }

  Stream<NoteDetailState> mapUpdateTitleEventToState(
      NoteDetailUpdateTitleEvent event) async* {
    yield NoteDetailEditedState(
      entity: state.entity..title = event.title,
    );
  }

  Stream<NoteDetailState> mapUpdateContentsEventToState(
      NoteDetailUpdateContentsEvent event) async* {
    yield NoteDetailEditedState(
      entity: state.entity..contents = event.contents,
    );
  }
}
