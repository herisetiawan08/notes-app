import 'package:notes/domain/entities/note_content_entity.dart';
import 'package:notes/domain/entities/note_item_entity.dart';

abstract class NoteDetailEvent {}

class NoteDetailSetEntityEvent extends NoteDetailEvent {
  final NoteItemEntity entity;

  NoteDetailSetEntityEvent({this.entity});
}

class NoteDetailUpdateTitleEvent extends NoteDetailEvent {
  final String title;
  NoteDetailUpdateTitleEvent({this.title});
}

class NoteDetailUpdateContentsEvent extends NoteDetailEvent {
  final List<NoteContentEntity> contents;
  NoteDetailUpdateContentsEvent({this.contents});
}
