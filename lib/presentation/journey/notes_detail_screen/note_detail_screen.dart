import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/constants/color_contants.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_bloc.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_event.dart';
import 'package:notes/presentation/journey/notes_detail_screen/bloc/note_detail_state.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_header.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_content_widget.dart';

class NoteDetailArguments {
  final NoteItemEntity note;

  NoteDetailArguments({
    this.note,
  });
}

class NoteDetailScreen extends StatefulWidget {
  @override
  _NoteDetailScreenState createState() => _NoteDetailScreenState();
}

class _NoteDetailScreenState extends State<NoteDetailScreen> {
  NoteDetailArguments arguments;
  NoteDetailBloc noteDetailBloc;

  @override
  initState() {
    super.initState();
    noteDetailBloc = NoteDetailBloc();
  }

  @override
  Widget build(BuildContext context) {
    arguments = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: BlocProvider<NoteDetailBloc>(
        create: (context) => noteDetailBloc
          ..add(
            NoteDetailSetEntityEvent(entity: arguments.note),
          ),
        child: BlocBuilder<NoteDetailBloc, NoteDetailState>(
          builder: (context, state) {
            if (state is NoteDetailInitState) {
              return Container();
            }
            return Container(
              color: state.entity.bgColor ?? ColorConstants.primaryColor,
              child: SafeArea(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(LayoutConstants.dimen_15),
                      child: NoteDetailHeader(
                        note: state.entity,
                      ),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                          child: Column(
                        children: state.entity.contents == null
                            ? [NoteContentWidget()]
                            : state.entity.contents
                                .map(
                                  (content) => NoteContentWidget(
                                    content: content,
                                  ),
                                )
                                .toList(),
                      )),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
