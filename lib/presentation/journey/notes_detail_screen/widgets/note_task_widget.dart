import 'package:flutter/material.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/domain/entities/note_content_entity.dart';
import 'package:notes/domain/entities/task_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_card_widget.dart';

class NoteTaskWidget extends StatefulWidget {
  final NoteContentTaskEntity content;

  const NoteTaskWidget({Key key, this.content}) : super(key: key);
  @override
  _NoteTaskWidgetState createState() => _NoteTaskWidgetState();
}

class _NoteTaskWidgetState extends State<NoteTaskWidget> {
  @override
  Widget build(BuildContext context) {
    return NoteCardWidget(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          ...widget.content.taskEntities.map(_buildTaskItem).toList(),
        ],
      ),
    );
  }

  Widget _buildTaskItem(TaskEntity task) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: LayoutConstants.dimen_5),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(task.checked ? Icons.check_circle : Icons.check_circle_outline),
          Text(task.text),
        ],
      ),
    );
  }
}
