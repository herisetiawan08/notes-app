import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:notes/common/constants/color_contants.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_constants.dart';
import 'package:notes/presentation/widgets/rubber_widget/navigation.dart';

class NoteDetailRubberWidget extends RubberModalRoute {
  final RouteSettings settings;
  NoteItemEntity note;
  ValueNotifier<Color> colorNotifier;
  ValueNotifier<Color> bgColorNotifier;
  ValueNotifier<String> category;

  NoteDetailRubberWidget(this.settings) {
    note = settings.arguments;
  }

  @override
  void install() {
    super.install();
    colorNotifier = ValueNotifier(note.titleColor ?? Colors.white);
    bgColorNotifier =
        ValueNotifier(note.bgColor ?? ColorConstants.primaryColor);
    category =
        ValueNotifier(note.category ?? NoteDetailLanguage.defaultCategory1);
  }

  @override
  Widget buildContent(BuildContext context, ScrollController controller) {
    return SingleChildScrollView(
      controller: controller,
      child: NoteDetailRubberBody(
        bgColorNotifier: bgColorNotifier,
        colorNotifier: colorNotifier,
        categoryNotifier: category,
      ),
    );
  }

  @override
  Widget buildHeader(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(LayoutConstants.dimen_15),
      child: Row(
        children: [
          Text(
            'Note Style',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: LayoutConstants.dimen_25,
            ),
          ),
          InkWell(
            onTap: () => Navigator.pop(
              context,
              note
                ..bgColor = bgColorNotifier.value
                ..titleColor = colorNotifier.value
                ..category = category.value,
            ),
            child: Text(
              'Save',
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get split => false;
}

class NoteDetailRubberBody extends StatefulWidget {
  final ValueNotifier<Color> colorNotifier;
  final ValueNotifier<Color> bgColorNotifier;
  final ValueNotifier<String> categoryNotifier;

  const NoteDetailRubberBody(
      {Key key,
      this.colorNotifier,
      this.bgColorNotifier,
      this.categoryNotifier})
      : super(key: key);
  @override
  _NoteDetailRubberBodyState createState() => _NoteDetailRubberBodyState();
}

class _NoteDetailRubberBodyState extends State<NoteDetailRubberBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(LayoutConstants.dimen_15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              DropdownButton<Color>(
                underline: Container(),
                icon: Icon(Icons.keyboard_arrow_down_rounded),
                hint: Text('Select title color you want'),
                items: NoteDetailConstants.colorSuggestion
                    .map(
                      (color) => DropdownMenuItem(
                        child: Text(color.name),
                        value: color.value,
                      ),
                    )
                    .toList(),
                onChanged: (color) => widget.colorNotifier.value = color,
              ),
              ValueListenableBuilder(
                valueListenable: widget.colorNotifier,
                builder: (context, value, _) => Container(
                  height: LayoutConstants.dimen_25,
                  width: LayoutConstants.dimen_25,
                  decoration: BoxDecoration(
                    color: value,
                    border: Border.all(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              DropdownButton<Color>(
                underline: Container(),
                icon: Icon(Icons.keyboard_arrow_down_rounded),
                hint: Text('Select background color you want'),
                items: NoteDetailConstants.bgColorSuggestion
                    .map(
                      (color) => DropdownMenuItem(
                        child: Text(color.name),
                        value: color.value,
                      ),
                    )
                    .toList(),
                onChanged: (color) => widget.bgColorNotifier.value = color,
              ),
              ValueListenableBuilder(
                valueListenable: widget.bgColorNotifier,
                builder: (context, value, _) => Container(
                  height: LayoutConstants.dimen_25,
                  width: LayoutConstants.dimen_25,
                  decoration: BoxDecoration(
                    color: value,
                    border: Border.all(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                ),
              ),
            ],
          ),
          ValueListenableBuilder(
            valueListenable: widget.categoryNotifier,
            builder: (context, value, _) => DropdownButton<String>(
              underline: Container(),
              icon: Icon(Icons.keyboard_arrow_down_rounded),
              hint: Text('Select category'),
              items: [...NoteDetailConstants.defaultCategories]
                  .map(
                    (_category) => DropdownMenuItem(
                      child: Text(_category),
                      value: _category,
                    ),
                  )
                  .toList(),
              onChanged: (_category) =>
                  widget.categoryNotifier.value = _category,
              value: value,
            ),
          ),
        ],
      ),
    );
  }
}
