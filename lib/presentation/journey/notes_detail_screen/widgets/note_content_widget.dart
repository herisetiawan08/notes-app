import 'package:flutter/material.dart';
import 'package:notes/domain/entities/note_content_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_plain_text_widget.dart';
import 'package:notes/presentation/journey/notes_detail_screen/widgets/note_task_widget.dart';

class NoteContentWidget extends StatefulWidget {
  final NoteContentEntity content;

  const NoteContentWidget({Key key, this.content}) : super(key: key);

  @override
  _NoteContentWidgetState createState() => _NoteContentWidgetState();
}

class _NoteContentWidgetState extends State<NoteContentWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.content == null) {
      return NotePlainTextWidget(
        content: widget.content,
      );
    }
    switch (widget.content.type) {
      case NoteContentType.plainText:
        return NotePlainTextWidget(
          content: widget.content,
        );
      case NoteContentType.task:
        return NoteTaskWidget(
          content: widget.content,
        );
      default:
        return Container();
    }
  }
}
