import 'package:flutter/material.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/presentation/journey/notes_screen/widgets/notes_item_widget.dart';

class NoteItemCategory extends StatelessWidget {
  final List<NoteItemEntity> notes;
  NoteItemCategory({this.notes});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: LayoutConstants.dimen_10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            notes.first.category,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500,
              fontSize: 25,
            ),
          ),
          ...notes
              .map((note) => NoteItemWidget(
                    note: note,
                  ))
              .toList(),
        ],
      ),
    );
  }
}
