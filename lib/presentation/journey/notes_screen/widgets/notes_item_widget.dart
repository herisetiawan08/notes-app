import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_bloc.dart';
import 'package:notes/common/bloc/notes_bloc/notes_event.dart';
import 'package:notes/common/constants/layout_contants.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/domain/entities/note_item_entity.dart';
import 'package:notes/presentation/journey/notes_detail_screen/note_detail_screen.dart';

class NoteItemWidget extends StatelessWidget {
  final NoteItemEntity note;

  const NoteItemWidget({Key key, this.note}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: LayoutConstants.dimen_5),
      child: InkWell(
        onTap: () async {
          final response = await Navigator.pushNamed(
            context,
            RouteList.noteDetail,
            arguments: NoteDetailArguments(
              note: note,
            ),
          );
          if (response is NoteItemEntity) {
            BlocProvider.of<NotesBloc>(context).add(
              UpdateNoteEvent(entity: response),
            );
          }
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(LayoutConstants.dimen_10),
            ),
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.all(LayoutConstants.dimen_15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  note.title,
                  style: TextStyle(
                    color: note.titleColor,
                    fontSize: LayoutConstants.dimen_15,
                  ),
                ),
                Icon(Icons.arrow_forward_ios),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
