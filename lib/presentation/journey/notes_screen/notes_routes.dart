import 'package:flutter/material.dart';
import 'package:notes/common/constants/routes_constants.dart';
import 'package:notes/presentation/journey/notes_screen/notes_screen.dart';

class NotesRoutes {
  static Map<String, WidgetBuilder> getAll() => {
    RouteList.notesList : (context) => NotesScreen(),
  };
}
